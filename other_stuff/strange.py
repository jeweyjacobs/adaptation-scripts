#14/09	This script is a sketch?
#	it takes every word from the input text
#	And makes a slideshow of frames of the movie
#

import os,subprocess
import PIL
import time
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFile
fnt = ImageFont.truetype("arial.ttf",150)
imput = 'scrn'
output = 'out/'
text= 'camus.txt'
i=0
wwrd=[]
widpic=400


lines = open( text, "r" ).readlines()
total = 0
for line in lines:
    words = line.split()
    for word in words:
        wwrd.append(word)

for imago in sorted(os.listdir(imput)):
  im = Image.new("RGB", (800, 600), "black")
  img=Image.open(imput+'/'+imago)
  ww= img.size[1] * widpic / img.size[0]
  ss = widpic,ww
  col= 255,255,255
  img=img.resize(ss,PIL.Image.ANTIALIAS)
  im.paste(img,(200,300))
  draw = ImageDraw.Draw(im)
  w, h = draw.textsize(wwrd[i],font=fnt)
  draw.text(((800-w)/2,50), wwrd[i], fill="white",font=fnt)
  i+=1
  draw = ImageDraw.Draw(img)
  im.save(output+str(i)+".jpg")
  os.system('feh -FD0.2 --cycle-once '+output+str(i)+'.jpg')
