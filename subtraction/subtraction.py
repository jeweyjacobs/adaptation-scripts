#!/usr/bin/env python

import re,os,subprocess,argparse,io,sys

reload(sys)
sys.setdefaultencoding('utf-8')

parser=argparse.ArgumentParser()
parser.add_argument('-c',action="store_true", help='uses, instead of complete silence, the first half second of sound in the movie, to substitute the dialogues', default=False, dest='silenz')
parser.add_argument('-g',action="store_true", help='adds a 1 second gap before and after the subtitle parts', default=False, dest='gap' )
parser.add_argument("moviefile", help='specify a movie file', type=str)
parser.add_argument('-p',action="store_true", help='print instead of make noise', default=False, dest='printo')
parser.add_argument('-s', type=str, dest='subtitle', help='Choose an .srt file to use')
parser.add_argument('-f', help="print to a file instead than to a printer", type=str, dest="printfile")
parser.add_argument('-t', help="select the book file", type=str, dest='bookfile')
results=parser.parse_args()

gap = 0
silenze = 0
prin = 0
prif = 0

subtitle=results.subtitle
file=results.moviefile

fileb=results.moviefile.split('.')[0]

if results.gap:
	gap=1
if results.silenz:
	silenze=1
if results.printo:
	prin=1
if results.printfile:
	prif=results.printfile
if results.bookfile:
	text=results.bookfile
	with io.open(text, 'r', encoding='utf8') as searchfile:
		searchfile=searchfile.read()
if results.subtitle:
	subtext=results.subtitle
	lines = open( subtext, "r" ).readlines()
total = 0
timeold= 0.0

output='outsound/'
if not os.path.exists(output):
    os.makedirs(output)
#file='visconti-the_stranger.wav'
#text='libro.txt'

allwords=[]
wordindex=[]
subwords=[]

#function textcross
#for linest in searchfile:
#	linespl=str(linest).split()
#	for word in linespl:
#		allwords.append(word)
	

def docross(searchfile, priff):
	regex = re.compile('[^a-zA-Z]')
	for linex in lines:
		if '-->' in linex:
			continue
		if linex == '\n' :
			continue
		if re.findall(r'[0-9]+\n', linex):
			continue
		else:
			linesw=str(linex).replace('<i>','').replace('</i>','')
			linesw=linesw.split()
			for subword in linesw:
				substrip=regex.sub('', subword)
				if substrip == "":
					continue
				if substrip == "\n":
					continue
				if substrip ==" ":
					continue
				zzzzz=0
				ccccc=0
				substrip=substrip.lower()
#				print 'searching for '+substrip
				while zzzzz==0:
					foundz=re.search(r"(?<!\w)%s(?!\w)" % substrip, searchfile.lower()[ccccc:])
					if foundz != None:
						foundz=foundz.start()

						print 'found '+substrip+' at ' + str(foundz) + ', ccccc=' + str(ccccc)
						#print searchfile.lower()[ccccc+foundz-1:ccccc+foundz+len(substrip)+1]
						if searchfile.lower()[ccccc+foundz-1:ccccc+foundz+len(substrip)+1] != ('"'+substrip+'"'):
							zzzzz=1
						else:
							ccccc=ccccc+foundz+len(substrip)
						#	print 'zzzzz!'
							foundz = None
					else:
						break
				if foundz != None:				
					foundz=foundz+ccccc
					#print substrip + ' should be equal to '+ searchfile[foundz:foundz+len(substrip)] + ' e a ' + str(foundz)
					add1="\n"					
					add2="\n"
					extra=0
					if (searchfile[foundz-3:foundz-1] == '"\n') or (searchfile[foundz-2:foundz] == '"\n'):
						add1=""
					#	print 'not adding before ' + substrip
					if (searchfile[foundz+len(substrip)+1:foundz+len(substrip)+5] == "\n.ST") or (searchfile[foundz+len(substrip)+2:foundz+len(substrip)+6] == "\n.ST"):
						add2="" 
					#	print 'not adding after ' + substrip
					#print add1+'.ST "'+searchfile[foundz:foundz+len(substrip)]+'"'+add2
					if searchfile[foundz-1]==" ":
						fffff=foundz
					elif searchfile[foundz-1]=='"':
						fffff=foundz-1
					else:
						fffff=foundz
					if searchfile[foundz+len(substrip)+1]==" ":
						extra=1
						eeeee=foundz+len(substrip)+2
		#			elif searchfile[foundz+len(substrip)+2]==" ":
		#				extra=2
		#				eeeee=foundz+len(substrip)+2
					
			#		elif searchfile[foundz+len(substrip)+1]=="\n":
			#			eeeee=foundz+len(substrip)+2
			#			extra=1	
					else:
						eeeee=foundz+len(substrip)+1
					searchfile= searchfile[:fffff]+add1+'.ST "'+searchfile[foundz:foundz+len(substrip)+extra]+'"'+add2+searchfile[eeeee:]
					searchfile=searchfile.replace('\n\n\n','\n \n \n').replace('\n\n','\n.br\n')

	with open("testttt.txt", 'w') as filez:
		pre= ".PAGE 21c 28c \n.T_MARGIN 2c \n.B_MARGIN 2.3c \n.L_MARGIN 2c \n.R_MARGIN 1.3c \n\n.de hd \n'sp 0.3c \n.tl '%'"+text+" with a subtraction from "+subtitle+" \n'sp 2.2c \n.. \n.wh 0 hd \n\n.de ST \n.nr ww \w'\\\$1' \n\Z@\\v'-.25m'\l'\\\\n[ww]u'@\\\$1 \n.. \n. \n\n"
		filez.write(pre+searchfile)		
	if prif==0:
		os.system('cat testttt.txt | groff -mom | lpr -P jve2')
	else:
		os.system('cat testttt.txt | groff -mom > '+prif)

#function sound
def dosound(xxx,silenz):
	timeold=0.0
	total=0
	for linex in lines:
		if '-->' in linex:
			line=linex.split()
			time1=line[0]
			time1=time1.split(',')
			milli1=int(time1[1])/1000
			time1=time1[0].split(':')
			hours1=int(time1[0])*3600
			minute1=int(time1[1])*60
			second1=int(time1[2])
			time1=hours1+minute1+second1+milli1-xxx
	
			time2=line[2]
	                time2=time2.split(',')
	                milli2=int(time2[1])/1000
	                time2=time2[0].split(':')
	                hours2=int(time2[0])*3600
	                minute2=int(time2[1])*60
	                second2=int(time2[2])
	                time2=hours2+minute2+second2+milli2+xxx
			length=time1-timeold
			if length > 0:
				os.system('sox '+fileb+' '+output+str(total).zfill(5)+'.wav trim '+str(timeold)+' '+str(length))
	                length2=time2-time1
			namesil=output+str(total+1).zfill(5)+'s.wav'
			if silenz == 0:
				os.system('sox -n -r 48000 -c 2 '+namesil+' trim '+str(time1)+' '+str(length2))
			else:
				os.system('sox '+fileb+' '+namesil+' trim 0.1 0.5 repeat '+str(length2*2))
			timeold=time2
			total+=2
		else:
			print linex
	os.system('sox '+fileb+' '+output+str(total).zfill(5)+'.wav trim '+str(time2))
	quantita=total//100
	for iii in range ( 0,quantita+1):
		os.system('sox '+output+'00'+str(iii)+'*.wav '+output+'0t'+str(iii)+'.wav')
	os.system('sox '+output+'0t*.wav '+output+'silenced-'+fileb)
	os.system('rm '+output+'0*.wav')
	os.system('vlc '+output+'silenced-'+fileb)

if prin==0:
	fileb=fileb+'.mp3'
	if not os.path.isfile(fileb):
		print 'Please wait, extracting audio channel'
		os.system('ffmpeg -i '+file+' -vn '+fileb)
	dosound(gap,silenze)
else:
	docross(searchfile, prif)
