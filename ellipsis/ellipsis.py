#!/usr/bin/env python

import datetime,re,os,subprocess
import subprocess
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("movie", help='specify a movie file', type=str)
parser.add_argument('-f', help="print to a file instead than to a printer", type=str, dest="printfile")
parser.add_argument('-p', help="specify printer name", type=str, dest="printer")
parser.add_argument('-t', help="select the book file", type=str, dest='text')
results=parser.parse_args()

text=results.text
printfile=results.printfile
movie=results.movie
printer=results.printer

searchfile = open(text, 'r').read().replace('\n','')
ell='...'
ell2='. . .'
ellips=[]
index=0
counter=0

fold='outellipse/'

if not os.path.exists(fold):
    os.makedirs(fold)
#movie='visconti-the_stranger.mpg'

proc = subprocess.Popen(["ffprobe", movie, "-show_format","-v","quiet"], stdout=subprocess.PIPE )
proc2 = subprocess.Popen(["grep","duration"], stdin=proc.stdout, stdout=subprocess.PIPE)
duration,err = proc2.communicate()
duration=duration.split('=')[1].replace('\n','')
dura=int(duration.split('.')[0])
libr=len(searchfile)
prop=float(dura)/float(libr)

while index < len(searchfile):
	oldindex=index
	fff=str(counter)
	index= searchfile.find(ell,index)
	if index == -1:
		break
	secs=index*prop
	secs= str(datetime.timedelta(seconds=secs))	
	filen= fold+fff
	print "ffmpeg -ss "+str(secs)+" -i "+movie+" -f image2 -vframes 1 "+filen+".jpg"
	os.system("ffmpeg -ss "+str(secs)+" -i "+movie+" -f image2 -vframes 1 "+filen+".jpg")	
	os.system("cat "+filen+".jpg | jpegtopnm | pnmtops -noturn > "+filen+".ps")
	counter+=1
	index+=3
	lastdot= searchfile[oldindex:index-5].rfind('.')
	lastdot=oldindex+lastdot
	lines= searchfile[lastdot+1:index]
	with open(filen+".txt", 'wb') as filez:
		if counter==1:
			pre=".PAGE 21c 29c \n.T_MARGIN 0c\n.B_MARGIN 0c\n.L_MARGIN 1c\n.R_MARGIN 0.5c\n.\\\"Page footer\n.de fo\n'sp 1v\n.tl \""+str(counter)+"\"ellipses in "+text+" and "+movie+"\n'bp\n..\n.wh 26c fo\n\n.ALD 6c\n"+lines+"\n\n.ALD 5c\n\n.PSPIC "+filen+".ps\n"
		else:
			pre=".PAGE 21c 29c \n.T_MARGIN 0c\n.B_MARGIN 0c\n.L_MARGIN 1c\n.R_MARGIN 0.5c\n.\\\"Page footer\n.de fo\n'sp 1v\n.tl \""+str(counter)+"\"\n'bp\n..\n.wh 26c fo\n\n.ALD 6c\n"+lines+"\n\n.ALD 5c\n\n.PSPIC "+filen+".ps\n"
		filez.write(pre)
	if printfile:
		os.system('cat '+filen+'.txt | groff -mom | ps2pdf - '+filen+'.part.pdf')
	else:
		os.system('cat '+filen+'.txt | groff -mom > '+filen+'print.ps')
if printfile:
	listz=0
	lista=''
	for f in os.listdir(fold):
		if f.endswith("part.pdf"):
			listz+=1
	for i in range (0,listz):
		lista=lista+str(i)+'.part.pdf '
	os.system('cd '+fold+'; pdftk '+lista+'output ../'+printfile+'.pdf')
	os.system('rm '+fold+'*')
#	os.system('cat '+filen+'.txt | groff -mom | lpr -P '+printer)
