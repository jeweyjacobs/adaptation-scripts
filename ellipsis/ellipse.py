#!/usr/bin/env python

import re,os,subprocess,argparse

parser=argparse.ArgumentParser()
parser.add_argument("folder", help='specify a folder with extracted frames', type=str)
parser.add_argument('-t', help="select the audio book file", type=str, dest='audio')
results=parser.parse_args()

folder=results.folder+'/'
prefix=[]
premo=[]
prev=''
add=0
#time0=44015
audiotrack=results.audio

for file in sorted(os.listdir(folder)):
	if file.endswith(".jpg"):
		pre=int(file.split('-')[0])
		filez=file.split('.')[0]
		fram=int(filez.split('-')[1].split('.')[0])
		time=int(fram/30)
	else:
		continue

	if pre not in prefix:
		prefix.append(pre)
		premo.append(time)
#		add=0
#	else:
#		if not ( (timv==time and (fram-frav)<11 ) or (time==timv+1 and (frav-fram>0)) or ( (timv%100)==59 and (frav-fram>0) )):
#			print timv,frav,time,fram
#			print time==timv,time==timv+1
#			add+=1
		#	prefix.append(str(add))
#			premo.append(time)		
#	if (add!=0):
#		newname= str(add)+pre+'-'+str(time)+'-'+str(fram)+'.jpg'
#		os.system('mv '+folder+file+' '+folder+newname)
	prev=filez
	frav=fram
	timv=time

for index, prefisso in enumerate(prefix):
#	starttime= str(premo[index])
#	starthour=int(starttime[-6:-4])
#	startmin=int(starttime[-4:-2])
#	startsec=int(starttime[-2:])
#	startsec=starthour*3600 + startmin*60 + startsec 
	startsec=premo[index]
	#- time0
	audiosec=float(startsec)
	#*2.85
	print audiosec
#	if prefisso == "67":
#		audiosec-=600
	os.system("ffmpeg -ss "+str(audiosec)+" -t 300 -i "+audiotrack+" "+folder+"tempfile.ogg")
	print "ffmpeg -ss "+str(audiosec)+" -t 300 -i "+audiotrack+" "+folder+"tempfile.ogg"
	os.system("ffmpeg -r 3 -pattern_type glob -i '"+folder+str(prefisso).zfill(3)+"-*.jpg' -i "+folder+"tempfile.ogg -map 0:v -map 1:a -c:a aac -shortest "+folder+str(prefisso)+".avi")
	os.system("rm "+folder+"tempfile.ogg")

